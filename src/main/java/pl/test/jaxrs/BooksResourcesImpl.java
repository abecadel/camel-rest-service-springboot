package pl.test.jaxrs;


import org.springframework.stereotype.Component;
import superbooks.Thebook;

import javax.ws.rs.core.Response;

/**
 * Created by Michal on 2015-07-13.
 */
@Component
public class BooksResourcesImpl implements application.BooksResource {
    @Override
    public Thebook getBookstoreid(String id) {
        return new Thebook();
    }

    @Override
    public Response postPut(Thebook thebook) {
        return Response.accepted().build();
    }
}
