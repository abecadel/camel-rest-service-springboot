package pl.test.jaxrs;

import application.BooksResource;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxrs.AbstractJAXRSFactoryBean;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import superbooks.Thebook;


/**
 * Created by Michal on 2015-07-13.
 */
@Configuration
@ImportResource({"classpath:META-INF/cxf/cxf.xml"})
public class ContextConfiguration {

    @Autowired
    private ApplicationContext ctx;

    @Bean
    public ServletRegistrationBean servletRegistrationBean(ApplicationContext context) {
        return new ServletRegistrationBean(new CXFServlet(), "/api/*");
    }

    @Bean
    public AbstractJAXRSFactoryBean helloRestServiceFactory(BooksResource booksResource) {
        Bus bus = (Bus) ctx.getBean(Bus.DEFAULT_BUS_ID);
        JAXRSServerFactoryBean endpoint = new JAXRSServerFactoryBean();
        endpoint.setBus(bus);
        endpoint.setAddress("/hello");
        endpoint.setServiceBean(booksResource);
        return endpoint;
    }

    @Bean
    RoutesBuilder myRouter() {
        return new RouteBuilder() {

            @Override
            public void configure() throws Exception {
                from("cxfrs:bean:helloRestServiceFactory")
                        .process(new Processor() {
                            public void process(Exchange exchange) throws Exception {
                                Thebook thebook = exchange.getIn().getBody(Thebook.class);
                                System.out.println(exchange.getIn().toString());
                            }
                        })
                        .setBody(constant("['SUCCESS']"));
            }

        };
    }


}
